
# chacha20-le32

An implementation of the ChaCha20 stream cipher.
This implementation is foremost meant for 32bit little-endian microcontrollers.


## Building

### For testing

There is a `main.c` file included which can be used for testing.
It will by default encrypt the plaintext example with the same
initial ChaCha20 state setup from RFC8439. It will output both
the example ciphertext bytes from RFC8439 and the resulting ciphertext from
chacha20-le32 for comparison.
You can build this by running `make`. Please see the `Makefile`.

### For inclusion in a project

Just include `chacha20-le32.h` in your C language source code file
and call function `CHACHA20_vInitState()` and `CHACHA20_vEncrypt()` with the
required parameters.

## Information

Please see RFC8439 for more information.

