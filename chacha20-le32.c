/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

This file is part of chacha20-le32, an implementation of the ChaCha20 stream cipher.

Copyright (c) 2021, Anton Harju
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* SPDX-License-Identifier: BSD-3-Clause */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "chacha20-le32.h"

#define COMPILER_TESTS 0


#if COMPILER_TESTS > 0

#if defined ( __GNUC__ ) && defined ( __ARM_ARCH_7M__ )
/* -march=armv7-m -mtune=cortex-m3 -mthumb */

#define CHACHA20_BLOCK_USE_REGISTERS 1
/* Ends up being essentially the same as the default with CHACHA20_BLOCK_USE_REGISTERS=1. */
#define ROL_N(a) ( 32 - (a) )
#define ROL(a, b) asm volatile ("ror %0, %1, %2" : "=r" (a) : "r" (a), "i" (b) )

#elif defined ( __ICCARM__ ) && defined ( IAR_ARM_CM3 )

#if 0
/* Was not able to test this. */
#define ROL_N(a) ( 32 - (a) )
#define ROL(a, b) asm volatile ("ROR %0, %1, %2" : "=r" (a) : "r" (a), "i" (b) )
#else
#define ROL_N(a) (a)
#define ROL(a, b) a = ( ( (a) << (b) ) | ( (a) >> ( 32 - (b) ) ) )
#endif
#define CHACHA20_BLOCK_USE_REGISTERS 0

#else

/* Let the compiler do what is best. */
#define ROL_N(a) (a)
#define ROL(a, b) a = ( ( (a) << (b) ) | ( (a) >> ( 32 - (b) ) ) )
#define CHACHA20_BLOCK_USE_REGISTERS 0

#endif


#else /* if COMPILER_TESTS > 0 */

#define ROL_N(a) (a)
#define ROL(a, b) a = ( ( (a) << (b) ) | ( (a) >> ( 32 - (b) ) ) )
#define CHACHA20_BLOCK_USE_REGISTERS 0

#endif /* if COMPILER_TESTS > 0 else */



#define BLOCKSIZE 64

#define QR(a, b, c, d) do {\
	a += b;\
	d ^= a;\
	ROL(d, ROL_N(16) );\
	c += d;\
	b ^= c;\
	ROL(b, ROL_N(12) );\
	a += b;\
	d ^= a;\
	ROL(d, ROL_N(8) );\
	c += d;\
	b ^= c;\
	ROL(b, ROL_N(7) );\
	} while(0)

static void _vChacha20Block (const uint32_t *pcu32InitialState, uint32_t *pu32State)
{
	uint8_t u8Ctr;
	uint32_t u32Block;

	u32Block = pu32State[12];

#if CHACHA20_BLOCK_USE_REGISTERS > 0
	register uint32_t ru32S0 = pu32State[0];
	register uint32_t ru32S1 = pu32State[1];
	register uint32_t ru32S2 = pu32State[2];
	register uint32_t ru32S3 = pu32State[3];
	register uint32_t ru32S4 = pu32State[4];
	register uint32_t ru32S5 = pu32State[5];
	register uint32_t ru32S6 = pu32State[6];
	register uint32_t ru32S7 = pu32State[7];
	register uint32_t ru32S8 = pu32State[8];
	register uint32_t ru32S9 = pu32State[9];
	register uint32_t ru32S10 = pu32State[10];
	register uint32_t ru32S11 = pu32State[11];
	register uint32_t ru32S12 = pu32State[12];
	register uint32_t ru32S13 = pu32State[13];
	register uint32_t ru32S14 = pu32State[14];
	register uint32_t ru32S15 = pu32State[15];
	u8Ctr = 10;
	do
	{
		/* Column round */
		QR( ru32S0, ru32S4, ru32S8, ru32S12 );
		QR( ru32S1, ru32S5, ru32S9, ru32S13 );
		QR( ru32S2, ru32S6, ru32S10, ru32S14 );
		QR( ru32S3, ru32S7, ru32S11, ru32S15 );

		/* Diagonal round */
		QR( ru32S0, ru32S5, ru32S10, ru32S15 );
		QR( ru32S1, ru32S6, ru32S11, ru32S12 );
		QR( ru32S2, ru32S7, ru32S8, ru32S13 );
		QR( ru32S3, ru32S4, ru32S9, ru32S14 );
	}
	while ( --u8Ctr );

	pu32State[0] = ru32S0 + pcu32InitialState[0];
	pu32State[1] = ru32S1 + pcu32InitialState[1];
	pu32State[2] = ru32S2 + pcu32InitialState[2];
	pu32State[3] = ru32S3 + pcu32InitialState[3];
	pu32State[4] = ru32S4 + pcu32InitialState[4];
	pu32State[5] = ru32S5 + pcu32InitialState[5];
	pu32State[6] = ru32S6 + pcu32InitialState[6];
	pu32State[7] = ru32S7 + pcu32InitialState[7];
	pu32State[8] = ru32S8 + pcu32InitialState[8];
	pu32State[9] = ru32S9 + pcu32InitialState[9];
	pu32State[10] = ru32S10 + pcu32InitialState[10];
	pu32State[11] = ru32S11 + pcu32InitialState[11];
	pu32State[12] = ru32S12 + u32Block;
	pu32State[13] = ru32S13 + pcu32InitialState[13];
	pu32State[14] = ru32S14 + pcu32InitialState[14];
	pu32State[15] = ru32S15 + pcu32InitialState[15];
#else

	u8Ctr = 10;
	do
	{
		/* Column round */
		QR( pu32State[0], pu32State[4], pu32State[8], pu32State[12] );
		QR( pu32State[1], pu32State[5], pu32State[9], pu32State[13] );
		QR( pu32State[2], pu32State[6], pu32State[10], pu32State[14] );
		QR( pu32State[3], pu32State[7], pu32State[11], pu32State[15] );

		/* Diagonal round */
		QR( pu32State[0], pu32State[5], pu32State[10], pu32State[15] );
		QR( pu32State[1], pu32State[6], pu32State[11], pu32State[12] );
		QR( pu32State[2], pu32State[7], pu32State[8], pu32State[13] );
		QR( pu32State[3], pu32State[4], pu32State[9], pu32State[14] );
	}
	while ( --u8Ctr );

	for ( u8Ctr = 0; u8Ctr < 12; u8Ctr++ )
	{
		pu32State[u8Ctr] += pcu32InitialState[u8Ctr];
	}
	pu32State[u8Ctr++] += u32Block;
	for ( ; u8Ctr < 16; u8Ctr++ )
	{
		pu32State[u8Ctr] += pcu32InitialState[u8Ctr];
	}
#endif

	return;
}

void CHACHA20_vInitState( uint32_t *pu32State, const uint32_t *pcu32Key, uint32_t u32Block, const uint32_t *pcu32Nonce )
{
	/* Constant */
	pu32State[0] = 0x61707865;
	pu32State[1] = 0x3320646E;
	pu32State[2] = 0x79622D32;
	pu32State[3] = 0x6B206574;
	/* Key */
	pu32State[4] = pcu32Key[0];
	pu32State[5] = pcu32Key[1];
	pu32State[6] = pcu32Key[2];
	pu32State[7] = pcu32Key[3];
	pu32State[8] = pcu32Key[4];
	pu32State[9] = pcu32Key[5];
	pu32State[10] = pcu32Key[6];
	pu32State[11] = pcu32Key[7];
	/* Block counter */
	pu32State[12] = u32Block;
	/* Nonce */
	pu32State[13] = pcu32Nonce[0];
	pu32State[14] = pcu32Nonce[1];
	pu32State[15] = pcu32Nonce[2];

	return;
}

void CHACHA20_vEncrypt( const uint32_t *pcu32InitialState, uint8_t *pu8Message, uint32_t u32MessageLen )
{
	uint32_t au32State[16];
	uint32_t u32NumMessageBlock;
	uint8_t u8NumRemainInt;
	uint8_t u8NumRemainByte;
	uint32_t u32Block;
	uint32_t *pu32MessagePtr;
	uint8_t *pu8MessagePtr;
	uint8_t *pu8StatePtr;
	uint32_t u32Ctr;
	uint8_t u8IntCtr;

	u32Block = pcu32InitialState[12];

	/* Amount of full 64 Byte blocks. */
	u32NumMessageBlock = u32MessageLen / BLOCKSIZE;

	/* If message length not multiple of 64, amount of full 4 byte integers in last block. */
	u8NumRemainInt = ( u32MessageLen % BLOCKSIZE ) / sizeof(uint32_t);

	/* If message length not multiple of 4, amount (1-3) of bytes at the end of message. */
	/* u8NumRemainByte = ( u32MessageLen % BLOCKSIZE ) % sizeof(uint32_t); */
	u8NumRemainByte = u32MessageLen % sizeof(uint32_t);

	for ( u32Ctr = 0; u32Ctr < u32NumMessageBlock; u32Ctr++ )
	{
		/* Keystream for full 64 byte block with index u32Ctr. */

		memcpy(&au32State[0], &pcu32InitialState[0], sizeof(uint32_t) * 16);
		au32State[12] = u32Block;
		_vChacha20Block( &pcu32InitialState[0], &au32State[0] );
		u32Block++;


		/* XOR 64 byte plaintext block with keystream one 4 byte integer at a time. */

		pu32MessagePtr = (uint32_t *) &pu8Message[ (u32Ctr * BLOCKSIZE) ];
		for ( u8IntCtr = 0; u8IntCtr < (BLOCKSIZE / 4); u8IntCtr++ )
		{
			*pu32MessagePtr ^= au32State[ u8IntCtr ];
			pu32MessagePtr++;
		}


		/* Repeat for all full blocks of 64 bytes. */
	}

	if ( u8NumRemainInt != 0 || u8NumRemainByte != 0 )
	{
		/* Message length is not a multiple of 64 byte block size.
		Keystream for last block. */

		memcpy(&au32State[0], &pcu32InitialState[0], sizeof(uint32_t) * 16);
		au32State[12] =  u32Block;
		_vChacha20Block( &pcu32InitialState[0], &au32State[0] );
		u32Block++;
		pu32MessagePtr = (uint32_t *) &pu8Message[ (u32NumMessageBlock * BLOCKSIZE) ];
	}

	for ( u8IntCtr = 0; u8IntCtr < u8NumRemainInt; u8IntCtr++ )
	{
		/* XOR last message block full 4 byte integers with keystream. */

		*pu32MessagePtr ^= au32State[ u8IntCtr ];
		pu32MessagePtr++;
	}

	if ( u8NumRemainByte != 0 )
	{
		/* Length of last block of message is not a multiple of 4.
		Set up to handle last 1-3 bytes of message separately. */

		pu8MessagePtr = (uint8_t *) &pu8Message[ (u32NumMessageBlock * BLOCKSIZE) + (u8NumRemainInt * sizeof(uint32_t)) ];
		pu8StatePtr = (uint8_t *) &au32State[ u8NumRemainInt ];
	}

	for ( u8IntCtr = 0; u8IntCtr < u8NumRemainByte; u8IntCtr++ )
	{
		*pu8MessagePtr ^= pu8StatePtr[ u8IntCtr ];
		pu8MessagePtr++;
	}

	return;
}
