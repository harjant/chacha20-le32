/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

This file is part of chacha20-le32, an implementation of the ChaCha20 stream cipher.

Copyright (c) 2021, Anton Harju
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* SPDX-License-Identifier: BSD-3-Clause */


void CHACHA20_vInitState( uint32_t *pu32State, const uint32_t *pcu32Key, uint32_t u32Block, const uint32_t *pcu32Nonce );
void CHACHA20_vEncrypt( const uint32_t *pcu32InitialState, uint8_t *pu8Message, uint32_t u32MessageLen );

