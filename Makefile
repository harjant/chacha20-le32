CC = gcc
LIBS = 
INCLUDES = 
CPPFLAGS = $(INCLUDES)
SOURCES = chacha20-le32.c main.c
OBJECTS = $(SOURCES:.c=.o)
DEFS = 
PROGRAM = chacha20-le32
DEPDIR = .deps
DEPF = $(DEPDIR)/$(*F)
MAKEDEPDIR = [ -d $(DEPDIR) ] || mkdir $(DEPDIR)
MAKEDEPEND = $(MAKEDEPDIR);gcc -M $(CPPFLAGS) -o $(DEPF).d $<

all : $(PROGRAM)

$(PROGRAM) : $(OBJECTS)
	$(CC) $(LIBS) -o $@ $(OBJECTS)

%.o : %.c
	@$(MAKEDEPEND); \
	  cp $(DEPF).d $(DEPF).P; \
	  sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	      -e '/^$$/ d' -e 's/$$/ :/' < $(DEPF).d >> $(DEPF).P; \
	  rm -f $(DEPF).d
	$(CC) $(INCLUDES) $(DEFS) -c $(@:.o=.c)

-include $(SOURCES:%.c=$(DEPDIR)/%.P)

clean:
	rm *.o $(DEPDIR)/*.P $(PROGRAM); \
	  rmdir $(DEPDIR)

.PHONY: clean all

