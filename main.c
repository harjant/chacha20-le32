/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

This file is part of chacha20-le32, an implementation of the ChaCha20 stream cipher.

Copyright (c) 2021, Anton Harju
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* SPDX-License-Identifier: BSD-3-Clause */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "chacha20-le32.h"


/*
This is the example initial state from RFC8439 as a constant for some specific tests.
The CHACHA20_vEncrypt() and _vChacha20Block() functions currently handle the block counter even if intial state is constant,
they don't handle the nonce however.
*/
const uint32_t c_au32Chacha20State[] =
{
	0x61707865, 0x3320646E, 0x79622D32, 0x6B206574, /* 4 constant  */
	0x03020100, 0x07060504, 0x0B0A0908, 0x0F0E0D0C, /* 4 key */
	0x13121110, 0x17161514, 0x1B1A1918, 0x1F1E1D1C, /* 4 key */
	0x00000001, 0x00000000, 0x4A000000, 0x00000000  /* 1 block counter, 3 nonce */
};


void _vChacha20EncryptTest( void )
{
	uint32_t u32Ctr;
	/* This is the example plaintext from RFC8439. */
	uint8_t au8Message[] = "Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the future, sunscreen would be it.";
	uint32_t u32MessageLen;
	uint32_t au32InitialState[16];
	/* This is the example initial state from RFC8439. */
	uint32_t au32Key[] = { 0x03020100, 0x07060504, 0x0B0A0908, 0x0F0E0D0C, 0x13121110, 0x17161514, 0x1B1A1918, 0x1F1E1D1C };
	uint32_t au32Nonce[] = { 0x00000000, 0x4A000000, 0x00000000 };
	uint32_t u32Block = 0x00000001;


	CHACHA20_vInitState( &au32InitialState[0], &au32Key[0], u32Block, &au32Nonce[0] );

	u32MessageLen = strlen(au8Message);
	CHACHA20_vEncrypt( &au32InitialState[0], au8Message, u32MessageLen );

	printf("Output of of chacha20_encrypt:\n");
	for ( u32Ctr = 0; u32Ctr < u32MessageLen; u32Ctr++ )
	{
		printf("%02x ", au8Message[ u32Ctr ] );
		if ( (u32Ctr + 1) % 32 == 0 )
		{
			printf("\n");
		}
	}

	printf("\nRFC 8439 Ciphertext example:\n");
	printf("6e 2e 35 9a 25 68 f9 80 41 ba 07 28 dd 0d 69 81 ");
	printf("e9 7e 7a ec 1d 43 60 c2 0a 27 af cc fd 9f ae 0b\n");
	printf("f9 1b 65 c5 52 47 33 ab 8f 59 3d ab cd 62 b3 57 ");
	printf("16 39 d6 24 e6 51 52 ab 8f 53 0c 35 9f 08 61 d8\n");
	printf("07 ca 0d bf 50 0d 6a 61 56 a3 8e 08 8a 22 b6 5e ");
	printf("52 bc 51 4d 16 cc f8 06 81 8c e9 1a b7 79 37 36\n");
	printf("5a f9 0b bf 74 a3 5b e6 b4 0b 8e ed f2 78 5e 42 ");
	printf("87 4d");
	printf("\n");

	return;
}


int main( void )
{
	_vChacha20EncryptTest();

	exit(0);
}
